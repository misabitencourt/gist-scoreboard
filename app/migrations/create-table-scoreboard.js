const db = require('../repos/db').sqlite;

module.exports = () => db.schema.createTable('scoreboard', table => {
    table.increments();
    table.integer('contest_id');
    table.integer('contestant_id');
    table.integer('penalties');
    table.integer('problem_id');
    table.integer('order');
    table.string('submit_type');
    
    table.timestamps();
});
