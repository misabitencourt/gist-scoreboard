const fs = require('fs');
const dbFile = './db/sqlite.db';

if (! fs.existsSync(dbFile)) {
    fs.writeFileSync(dbFile, '');
}

const sqlite = require('knex')({
    client: 'sqlite3',
    connection: {
        filename: dbFile
    }
});


module.exports = {sqlite}