const fetch = require('node-fetch');
const config = require('../config');
const url = 'https://api.github.com';
const authorizeUrl = 'https://github.com/login/oauth/authorize';
const btoa = require('btoa');
const auth = `${config.GITHUB_USERNAME}:${config.GITHUB_PASSWORD}`;

module.exports.authorizeUrl = scope => {
    scope = encodeURIComponent(scope.join(' '));

    return `${authorizeUrl}?client_id=${config.GITHUB_OAUTH_CLIENT_ID}&scope=${scope}`;
};


module.exports.get = method => fetch(`${url}${method}`, {
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${btoa(auth)}`
    }
}).then(res => {
    if (res.status !== 200) {
        throw {githubStatus: res.status, msg: res.text()};
    }

    return res.json()
});


module.exports.post = (method, json) => fetch(`${url}${method}`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Basic ${btoa(auth)}`
    },
    body: JSON.stringify(json)
}).then(res => {
    if (res.status !== 200 && res.status !== 201) {
        throw {githubStatus: res.status, msg: 'Github error'};
    }

    return res.json()
}).catch(err => {
    console.error(err);
});


module.exports.getAccessToken = code => {
    return fetch(`https://github.com/login/oauth/access_token`, {
        headers: {
            'Content-Type': 'application/json'
        },  
        method: 'POST',
        body: JSON.stringify({
            client_id: config.GITHUB_OAUTH_CLIENT_ID,
            client_secret: config.GITHUB_OAUTH_CLIENT_SECRET,
            code
        })
    }).then(res => {
        if (res.status !== 200) {
            throw {status: res.status, msg: 'Github error'};
        }

        return res.text().then(text => {
            text = text || '';
            const accessToken = text.split('access_token=').pop().split('&').shift();

            return {accessToken};
        }).catch(err => {
            throw err;
        });
    })
}
