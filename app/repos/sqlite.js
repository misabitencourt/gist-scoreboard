const db = require('./db').sqlite;

module.exports.create = ({modelName, tx, newRegister}) => {
    return (tx ? db(modelName).transacting(tx) : db(modelName)).insert(newRegister);
};

module.exports.transaction = cb => db.transaction(cb);

module.exports.list = ({modelName}) => {
    let query = db(modelName);
    
    return query.orderBy('id', 'desc').limit(15);
}

module.exports.retrieve = ({modelName, filters, params, select, groupBy, 
                            sum, from, leftJoins, orderBy, tx, limit=100}) => {
    let whereSql = filters || '';

    let query = tx ? db(modelName).transacting(tx) : db(modelName);
    if (sum) {
        query = query.sum(sum);
    }
    if (select) {
        query = query.select(select);
    }
    if (from) {
        query = query.from(from);
    }
    if (leftJoins) {
        leftJoins.forEach(lj => {
            query = query.leftJoin(lj.table, lj.localField, lj.foreignField);
        });
    }
    if (groupBy) {
        query = query.groupBy(groupBy);
    }
    if (orderBy)  {
        query = query.orderBy(orderBy);
    }

    if (whereSql) {
        whereSql = `1=1 AND (${whereSql})`;
    }

    return query.whereRaw(whereSql, params || []).limit(limit);
};

module.exports.update = ({modelName, id, values}) => db(modelName).where('id', '=', id).update(values);

module.exports.destroy = ({modelName, id}) => db(modelName).where('id', '=', id).del();

