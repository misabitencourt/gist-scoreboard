const repo = require('../repos/github-api');

function validateFiles(files) {
    let badTyping = false;

    for (let file of files) {
        if (typeof(file) !== 'object') {
            badTyping = true;
            continue;
        }

        if (! file.name) {
            badTyping = true;
        }

        if (file.name.split('.').length !== 2) {
            badTyping = true;
        }

        if (! file.content) {
            badTyping = true;
        }
    }

    if (badTyping) {
        throw 'Files passed in wrong format';
    }
}

exports.listComments = gistId => {
    return repo.get(`/gists/${gistId}/comments`).then(comments => {
        return comments.map(comment => {
            return {
                id: comment.id,
                node: comment.node_id,
                url: comment.url,
                text: comment.body,
                user: (comment.user || {}).login || 'unknow',
                created_at: new Date(comment.created_at),
                updated_at: new Date(comment.updated_at)
            };
        })
    });
};


exports.create = ({gist, githubAccessToken}) => {
    if (! gist) {
        throw 'No gist passed through';
    }

    if (! gist.description) {
        throw 'No gist description passed through';
    }

    if (! (gist.files && gist.files.length)) {
        throw 'No one file was passed through';
    }

    validateFiles(gist.files);

    const files = {};
    gist.files.forEach(file => files[file.name] = {content: file.content});
    gist.files = files;
    gist.public = gist.public !== false; // default is true

    return repo.post('/gists', gist, githubAccessToken)
} 