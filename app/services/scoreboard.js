const repo = require('../repos/sqlite');
const MAX_PROBLEMS = 9;
const MAX_CONTESTANTS = 100;
const TIMEOUT_PENALTY = 20;

const submitTypes = [
    {id: 1, letter: 'C', label: 'Correct', score: true},
    {id: 2, letter: 'I', label: 'Incorrect'},
    {id: 3, letter: 'R', label: 'Classification Request', skip: true},
    {id: 4, letter: 'U', label: 'Unjudged', skip: true}
]

const correctSubmitId = submitTypes.find(st => st.score);
const skipSubmitIds = submitTypes.filter(st => st.skip).map(st => st.letter);

function scorePoints(score, list, cases=null) {
    if (skipSubmitIds.indexOf(score.submit_type) !== -1) {
        return;
    }

    if (score.contestant_id > MAX_CONTESTANTS) {
        throw 'Incorrect contestant id';
    }

    const correct = score.submit_type === correctSubmitId.letter;
    let contestantScore = list.find(i => i.contestantId === score.contestant_id);
    if (! contestantScore) {
        list.push(contestantScore = {contestantId: score.contestant_id, scoring: 0});
    }

    contestantScore.scoring = contestantScore.scoring || 0;
    contestantScore.penalties = contestantScore.penalties || 0;
    contestantScore.extraPenalties = contestantScore.extraPenalties || 0;
    contestantScore.penalties = contestantScore.penalties > score.penalties ? 
                                    contestantScore.penalties : score.penalties;
    if (correct) {
        contestantScore.scoring += 1;
        if (contestantScore.incorrectSent) {
            contestantScore.extraPenalties += contestantScore.penalties + TIMEOUT_PENALTY;
            contestantScore.incorrectSent = false;
        }
    } else {
        contestantScore.incorrectSent = true;
    }
}

function validate(score) {
    if (! score) {
        throw 'No score passed through';
    }

    if (! score.contestant_id) {
        throw 'No contestant passed through';
    }

    if (score.problem_id === undefined) {
        throw 'No problem id passed through';
    }

    if (score.penalties === undefined) {
        throw 'No penalty time count passed through';
    }

    if (! submitTypes.find(st => st.letter === score.submit_type)) {
        throw 'Wrong submit type was passed through';
    }
}

exports.create = scores => new Promise((resolve, reject) => {
    if (! scores.contest_id) {
        return reject('No contest id passed through'); 
    }

    if (! scores.cases) {
        return reject('No cases count passed through'); 
    }

    if (! (scores.list && scores.list.length)) {
        return reject('No scores passed through'); 
    }

    repo.transaction(tx => {
        const scoring = [];

        for (let score of scores.list) {
            try {
                validate(score);
                scorePoints(score, scoring, score.cases);
                score.contest_id = scores.contest_id;
            } catch (e) {
                return reject(e);
            }
        }

        Promise.all(scores.list.map((score, index) => {
            score.order = index;
            return repo.create({
                tx,
                modelName: 'scoreboard',
                newRegister: score
            });
        })).then(() => {
            tx.commit();
            resolve();
        }).catch(reject);
    });
})

exports.scoring = contestId => {
    return repo.retrieve({
        modelName: 'scoreboard',
        filters: 'contest_id = :contestId',
        params: {contestId},
        orderBy: 'order'
    }).then(scores => {
        const scoreResume = [];
        scores.forEach(score => scorePoints(score, scoreResume));
        scoreResume.forEach(resume => resume.penalties += resume.extraPenalties);

        return { contestId, output: scoreResume };
    });
}