const repo = require('../repos/github-api');

exports.getAccessToken = code => repo.getAccessToken(code);
exports.getAuthorizeUrl = scope => repo.authorizeUrl(scope);