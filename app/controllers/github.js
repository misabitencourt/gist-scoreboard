const service = require('../services/github');
const url  = require('url');

module.exports = app => {
    app.get('/github/login/', (req, res) => res.redirect(
        service.getAuthorizeUrl(['gist'])
    ));

    app.get('/github/login/access-token', (req, res) => {
        const url_parts = url.parse(req.url, true);
        const query = url_parts.query;
        const code = query.code;

        if (! code) {
            return res.status(403).json({msg: 'Invalid code'});
        }

        service.getAccessToken(code).then(auth => res.json(auth))
                                    .catch(err => res.status(500).json(err));
    });
}