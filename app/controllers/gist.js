const service = require('../services/gist');

module.exports = app => {
    app.get('/github/gist/:id/comments/', (req, res) => {
        const id = req.params.id;
        service.listComments(id).then(comments => {
            if (! comments.length) {
                return res.sendStatus(204);
            }
            res.json(comments);
        }).catch(err => {
            res.status(500)
               .json(err);
        });
    });

    app.post('/github/gist/', (req, res) => {
        service.create({gist: req.body}).then(gist => res.json(gist))
                                                           .catch(err => res.status(500).json(err));
    });
};