const service = require('../services/scoreboard');
const EOL = require('os').EOL;

module.exports = app => {
    app.post('/scoreboard/', (req, res) => {
        const reqBody = Object.assign({}, req.body);
        reqBody.entry = (reqBody.entry || '').trim();
        const lines = reqBody.input.split(EOL);
        const scores = {
            contest_id: req.body.contestId,
            cases: (lines.shift() || '').trim()*1,
            list: lines.filter(l => l.trim()).map(line => {
                const scoreArr = (line || '').trim().split(' ');
                return {
                    contestant_id: scoreArr[0]*1,
                    problem_id: scoreArr[1]*1,
                    penalties: scoreArr[2]*1,
                    submit_type: scoreArr[3]
                }
            })
        };

        service.create(scores).then(() => res.sendStatus(200))
                              .catch(err => res.status(500).json(err));
    });

    app.get('/scoreboard/:contestId/scoring/', (req, res) => {
        service.scoring(req.params.contestId).then(scoring => {
            scoring.output = scoring.output.reduce((old, scoreResume) => {
                return `${old}${EOL}${scoreResume.contestantId} ${scoreResume.scoring} ${scoreResume.penalties}`;
            }, '');
            res.json(scoring);
        }).catch(err => {
            res.status(500).json(err);
        });
    });
};