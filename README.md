# Gist scoreboard

Http API that create/retrieve github gists and create/retrieve a contest scoreboard.

## Requirements

SO with [Nodejs](https://nodejs.org/en/) on version 8 or superior installed.

## Installing

Make sure if you have an empty file called migrated.dat in the project root and run:

```
npm install
```

## Starting

Make sure if your port 3000 is free to use and run:

```
npm start
```

## Configuring

In the project root, there is a .env file to configure github API credentials and others projects
configurations.

## Testing

Unit tests is on test directory (of course). To run it, type it on your console:

```
npm run test
```

Some Postman collection with API tests are saved in test directory.

