const assert = require('assert');
const request = require('supertest');
const app = require('../../index');

module.exports = () => new Promise((resolve, reject) => {
    describe('Github', () => {  

        describe('Api Test', () => {
            it('List and create gist comments', function(done) {
                this.timeout(1e4);

                request(app)
                .get('/github/gist/d530428f8f7115c7a3f956b7719915b3/comments/')
                .expect(200)
                .end(err => {
                    if (err) throw err;
                    request(app)
                    .post('/github/gist/')
                    .send({
                        "description": "A simple gist file",
                        "files": [
                            {
                                "name": "hello.js",
                                "content": "console.log('Hello world!');"
                            }
                        ]
                    })
                    .expect(200)
                    .end(err => {
                        if (err) throw err;
                        done();
                        resolve();
                    });
                });
            });
        });
    });
})