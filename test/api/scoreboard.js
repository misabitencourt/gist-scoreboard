const assert = require('assert');
const request = require('supertest');
const app = require('../../index');

module.exports = () => new Promise((resolve, reject) => {
    describe('Scoreboard', () => {  

        describe('Api Test', () => {
            it('should validate, save the sample scoreboard', (done) => {
                request(app)
                    .post('/scoreboard/')
                    .expect('Content-Type', /json/)
                    .send({
                        "contestId": 1,
                        "input": "1"
                    })
                    .expect(500)
                    .end((err, res) => {
                        if (err) throw err;
                        assert(res, 'No scores passed through');
    
                        // Post Valid Scoreboard
                        request(app)
                        .post('/scoreboard/')
                        .send({
                            "contestId": 1,
                            "input": "1\r\n1 2 10 I\r\n3 1 11 C\r\n1 2 19 R\r\n1 2 21 C\r\n1 1 25 C\r\n"
                        })
                        .expect(200)
                        .end((err, res) => {
                            if (err) throw err;
    
                            // Get Scoreboard
                            request(app)
                            .get('/scoreboard/1/scoring/')
                            .expect('Content-Type', /json/)
                            .send({
                                "contestId": 1,
                                "input": "1\r\n1 2 10 I\r\n3 1 11 C\r\n1 2 19 R\r\n1 2 21 C\r\n1 1 25 C\r\n"
                            })
                            .expect(200)
                            .end((err, res) => {
                                if (err) throw err;
                                done();
                                resolve();
                            });
                        });
                    });
                });
            });
    });
})